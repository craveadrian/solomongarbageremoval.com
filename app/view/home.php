<div id="section1" class="resSection">
	<div class="row">
		<div class="services resFlex">
			<dl class="col-4">
				<dt> <img src="public/images/content/svc1.jpg" alt="Service Icons"> </dt>
				<dd>Family Owned</dd>
			</dl class="col-4">
			<dl>
				<dt> <img src="public/images/content/svc2.jpg" alt="Service Icons"> </dt>
				<dd>Fast & Reliable Service</dd>
			</dl>
			<dl class="col-4">
				<dt> <img src="public/images/content/svc3.jpg" alt="Service Icons"> </dt>
				<dd>Licensed & Insured</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section2" class="resSection">
	<div class="row">
		<div class="s2Left col-6 fl resSection">
			<section>
				<h1><span>Garbage Removal<br> in Far Rockaway, NY</span></h1>
				<p>Protecting the environment only takes a few small steps a day, and recycling is a great place to start. By collecting your plastics and aluminum cans instead of throwing them away, you can help save the earth. Piling up those recyclables can leave behind a big mess. Don’t try sorting them on your own and allow Solomon Garbage Removal & Handyman Services to help.</p>
			</section>
		</div>
		<div class="s2Right col-6	fl">
			<img src="public/images/content/section2Img.jpg" alt="Man" class="resImage">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section3" class="resSection">
	<div class="row">
		<div class="s3Left col-6 fl">
			<img src="public/images/content/section3Img.jpg" alt="Man" class="resImage">
		</div>
		<div class="s3Right col-6 fl resSection">
			<section>
				<h1><span>About<br> Us</span></h1>
				<p>Our garbage removal and collection service comes to your home and takes care of everything. Our handyman services streamlines the process to make it convenient, easy, and fast. Trash disposal is best left to the professionals. It’s a dirty job, but someone’s gotta do it.</p>
				<p>We take pride in our service. We never want you to worry. If you any questions about our garbage and scrap metal removal services, please give us a call and ask. We are happy to answer your questions. If you want to start recycling more effectively, call Solomon Garbage Removal & Handyman Services in Far Rockaway, NY today!</p>
			</section>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section4" class="resSection">
	<div class="row">
		<div class="s4Left col-4 fl resSection">
			<section>
				<h1><span>OUR <br>SERVICES</span></h1>
				<p>If you want to start recycling more effectively, call Solomon Garbage Removal & Handyman Services in Far Rockaway, NY today!</p>
				<a href="<?php echo URL ?>services#content" class="btn">view more</a>
			</section>
		</div>
		<div class="s4Right col-8 fl resFlex">
			<dl>
				<dt><img src="public/images/content/service1.jpg" alt="Service Image"></dt>
				<dd>PLUMBING <br>SERVICES</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/service2.jpg" alt="Service Image"></dt>
				<dd>GARBAGE <br>REMOVAL</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/service3.jpg" alt="Service Image"></dt>
				<dd>DRYWALL <br>INSTALLATION</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/service4.jpg" alt="Service Image"></dt>
				<dd>RESIDENTIAL <br>PAINTING</dd>
			</dl>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section5" class="resSection">
	<div class="row">
		<section>
			<h2>WE SPECIALIZE</h2>
			<p>in waste material removal, however we provide residential handyman services as well</p>
		</section>
	</div>
</div>
<div id="section6" class="resSection">
	<div class="row">
		<div class="images">
			<h1> <span>Our <br> Works</span> </h1>
			<img src="public/images/content/work1.jpg" alt="Works Image">
			<img src="public/images/content/work2.jpg" alt="Works Image">
			<img src="public/images/content/work3.jpg" alt="Works Image">
			<img src="public/images/content/work4.jpg" alt="Works Image">
			<img src="public/images/content/work5.jpg" alt="Works Image">
			<img src="public/images/content/work6.jpg" alt="Works Image">
			<img src="public/images/content/work7.jpg" alt="Works Image">
			<img src="public/images/content/work8.jpg" alt="Works Image">
		</div>
	</div>
</div>
<div id="section7" class="resSection">
	<div class="row">
		<div class="s7Left col-3 fl resSection">
			<h1> <span>Client <br> Reviews</span> </h1>
		</div>
		<div class="s7Mid col-1 fl resImage">
			<img src="public/images/sprite.png" alt="quote" class="bg-quote">
			<p>
				<a href="">	<img src="public/images/sprite.png" alt="quote" class="bg-arrowLeft"></a>
				<a href="">	<img src="public/images/sprite.png" alt="quote" class="bg-arrowRightHover"></a>
			</p>
		</div>
		<div class="s7Right col-8 fl">
			<div class="quote">
				<div class="qtext col-8 fl">
					<p>I had the pleasure of using this service to remove garbage from my garage. Solomon was very pleasant. The garbage was removed quickly, my garage was left clean and the service was fairly priced. I highly recommend this company and I would gladly use them again.</p>
					<p class="author"><span>&#9733; &#9733; &#9733; &#9733; &#9733;</span>-JOHN DOE</p>
				</div>
				<img src="public/images/content/reviewImg.jpg" alt="profile" class="resImage fr">
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section8" class="resSection">
	<div class="row">
		<div class="s8Left col-5 fl">
			<img src="public/images/content/svcAreaImg.jpg" alt="FLoor" class="resImage">
		</div>
		<div class="s8Right col-6 fl resSection">
			<section>
				<h1> <span>Service <br>Areas</span> </h1>
				<ul>
					<li>Brooklyn</li>
					<li>Jamaica</li>
					<li>Far Rockaway</li>
					<li>Flushing</li>
					<li>Kew Gardens</li>
					<li>Queens Village</li>
					<li>Ridgewood</li>
					<li>Saint Albans</li>
					<li>South Ozone Park</li>
					<li>Cedarhurst</li>
					<li>Bayside</li>
					<li>Rockaway Park</li>
				</ul>
			</section>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

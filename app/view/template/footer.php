<footer>
	<div id="footer">
		<div class="footTop">
			<div class="row">
				<div class="footLeft col-4 fl">
					<section>
						<a href="<?php echo URL ?>"> <img src="public/images/common/footLogo.png" alt="Logo" class="footLogo"> </a>
						<p>Solomon Garbage Removal is an owner operated business that has been servicing the 5 Burroughs for north of 15 years. We specialize in waste material removal, however we provide residential handyman services as well. Please do not hesitate to give us a call for an estimate.</p>
					</section>
				</div>
				<div class="footMid col-3 fl">
					<section>
						<h3>CONTACT INFO</h3>
						<p><img src="public/images/sprite.png" alt="location" class="bg-location"><span><?php $this->info("address"); ?></span></p>
						<p><img src="public/images/sprite.png" alt="phone" class="bg-phone"><span><?php $this->info(["phone","tel"]); ?></span></p>
						<p><img src="public/images/sprite.png" alt="email" class="bg-email"><span><?php $this->info(["email","mailto"]); ?></span></p>
						<p>
							<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">F</a>
							<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">L</a>
							<a href="<?php $this->info("yt_link") ?>" class="socialico" target="_blank">X</a>
							<a href="<?php $this->info("rss_link") ?>" class="socialico" target="_blank">R</a>
						</p>
					</section>
				</div>
				<div class="footRight col-5 fr">
					<section>
						<h3>GET IN TOUCH</h3>
						<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
							<div class="col-6 fl">
								<label><span class="ctc-hide">Name</span>
									<input type="text" name="name" placeholder="Name:">
								</label>
							</div>
							<div class="col-6 fr">
								<label><span class="ctc-hide">Email</span>
									<input type="text" name="email" placeholder="Email:">
								</label>
							</div>
							<div class="clearfix"></div>
							<label><span class="ctc-hide">Message</span>
								<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
							</label>
							<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
							<div class="g-recaptcha"></div>
							<label>
								<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
							</label><br>
							<?php if( $this->siteInfo['policy_link'] ): ?>
							<label>
								<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
							</label>
							<?php endif ?>
							<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
						</form>
					</section>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="footBot">
			<div class="row">
				<p class="copy">
					<?php $this->info("company_name"); ?> © <?php echo date("Y"); ?>. All Rights Reserved.
					<?php if( $this->siteInfo['policy_link'] ): ?>
						<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
					<?php endif ?>
				</p>
				<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
			</div>
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>

<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,800,900|Oswald|Poppins" rel="stylesheet">
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop">
				<div class="row">
					<div class="hdTopLeft col-4 fl">
						<p>
							<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">F</a>
							<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">L</a>
							<a href="<?php $this->info("yt_link") ?>" class="socialico" target="_blank">X</a>
							<a href="<?php $this->info("rss_link") ?>" class="socialico" target="_blank">R</a>
						</p>
					</div>
					<div class="hdTopMid col-4 fl">
						<p> <img src="public/images/sprite.png" class="bg-phone" alt="phone icon"> <span><?php $this->info(["phone","tel"])?></span> </p>
					</div>
					<div class="hdTopRight col-4 fr">
						<p> <img src="public/images/sprite.png" class="bg-email" alt="email icon"> <span><?php $this->info(["email","mailto"])?></span> </p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<div class="hdBotLeft col-3 fl">
						<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="Logo" class="logo"> </a>
					</div>
					<div class="hdBotRight col-9 fl">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about">ABOUT US</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
							</ul>
						</nav>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner" class="resSection">
			<div class="row">
				<div class="tag col-4 fr">
					<p>WE TAKE <span>PRIDE</span> IN OUR SERVICE</p>
					<p>Our garbage removal and collection service comes to your home and takes care of everything.</p>
					<a href="<?php echo URL ?>contact#content" class="button">FREE ESTIMATE</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	<?php endif; ?>
